import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:kitchen_mart/Bdeal_itempage/bdeal1.dart';
import 'package:kitchen_mart/Bdeal_itempage/bdeal2.dart';
import 'package:kitchen_mart/Bdeal_itempage/bdeal3.dart';
import 'package:kitchen_mart/Bdeal_itempage/bdeal4.dart';
import 'package:kitchen_mart/Bdeal_itempage/bdeal5.dart';
import 'package:kitchen_mart/Bdeal_itempage/bdeal6.dart';
import 'package:kitchen_mart/Specialty_itempage/st1.dart';
import 'package:kitchen_mart/Specialty_itempage/st2.dart';
import 'package:kitchen_mart/Specialty_itempage/st3.dart';
import 'package:kitchen_mart/Specialty_itempage/st4.dart';
import 'package:kitchen_mart/Specialty_itempage/st5.dart';
import 'package:kitchen_mart/Specialty_itempage/st6.dart';
import 'package:kitchen_mart/Specialty_itempage/st7.dart';
import 'package:kitchen_mart/Specialty_itempage/st8.dart';
import 'package:kitchen_mart/appliances_itempage/apl1.dart';
import 'package:kitchen_mart/appliances_itempage/apl10.dart';
import 'package:kitchen_mart/appliances_itempage/apl11.dart';
import 'package:kitchen_mart/appliances_itempage/apl2.dart';
import 'package:kitchen_mart/appliances_itempage/apl3.dart';
import 'package:kitchen_mart/appliances_itempage/apl4.dart';
import 'package:kitchen_mart/appliances_itempage/apl5.dart';
import 'package:kitchen_mart/appliances_itempage/apl6.dart';
import 'package:kitchen_mart/appliances_itempage/apl7.dart';
import 'package:kitchen_mart/appliances_itempage/apl8.dart';
import 'package:kitchen_mart/appliances_itempage/apl9.dart';
import 'package:kitchen_mart/baking_itempage/bt1.dart';
import 'package:kitchen_mart/baking_itempage/bt10.dart';
import 'package:kitchen_mart/baking_itempage/bt2.dart';
import 'package:kitchen_mart/baking_itempage/bt3.dart';
import 'package:kitchen_mart/baking_itempage/bt4.dart';
import 'package:kitchen_mart/baking_itempage/bt5.dart';
import 'package:kitchen_mart/baking_itempage/bt6.dart';
import 'package:kitchen_mart/baking_itempage/bt7.dart';
import 'package:kitchen_mart/baking_itempage/bt8.dart';
import 'package:kitchen_mart/baking_itempage/bt9.dart';
import 'package:kitchen_mart/below_itempage/Items1.dart';
import 'package:kitchen_mart/below_itempage/Items2.dart';
import 'package:kitchen_mart/below_itempage/Items3.dart';
import 'package:kitchen_mart/below_itempage/Items4.dart';
import 'package:kitchen_mart/below_itempage/Items5.dart';
import 'package:kitchen_mart/below_itempage/Items6.dart';
import 'package:kitchen_mart/below_itempage/Items7.dart';
import 'package:kitchen_mart/cooking_itempage/ct1.dart';
import 'package:kitchen_mart/cooking_itempage/ct10.dart';
import 'package:kitchen_mart/cooking_itempage/ct2.dart';
import 'package:kitchen_mart/cooking_itempage/ct3.dart';
import 'package:kitchen_mart/cooking_itempage/ct4.dart';
import 'package:kitchen_mart/cooking_itempage/ct5.dart';
import 'package:kitchen_mart/cooking_itempage/ct6.dart';
import 'package:kitchen_mart/cooking_itempage/ct7.dart';
import 'package:kitchen_mart/cooking_itempage/ct8.dart';
import 'package:kitchen_mart/cooking_itempage/ct9.dart';
import 'package:kitchen_mart/cutting_itempage/cc1.dart';
import 'package:kitchen_mart/cutting_itempage/cc2.dart';
import 'package:kitchen_mart/cutting_itempage/cc3.dart';
import 'package:kitchen_mart/cutting_itempage/cc4.dart';
import 'package:kitchen_mart/cutting_itempage/cc5.dart';
import 'package:kitchen_mart/cutting_itempage/cc6.dart';
import 'package:kitchen_mart/cutting_itempage/cc7.dart';
import 'package:kitchen_mart/cutting_itempage/cc8.dart';
import 'package:kitchen_mart/mixing_itempage/mp1.dart';
import 'package:kitchen_mart/mixing_itempage/mp2.dart';
import 'package:kitchen_mart/mixing_itempage/mp3.dart';
import 'package:kitchen_mart/mixing_itempage/mp4.dart';
import 'package:kitchen_mart/mixing_itempage/mp5.dart';
import 'package:kitchen_mart/mixing_itempage/mp6.dart';
import 'package:kitchen_mart/mixing_itempage/mp7.dart';
import 'package:kitchen_mart/searchfilter/product_data.dart';
import 'package:kitchen_mart/widgets/SaleWidget.dart';
import 'package:kitchen_mart/widgets/DealsWidget.dart';
import 'package:kitchen_mart/widgets/ItemWidget.dart';
import 'package:kitchen_mart/appbar/homeappbar.dart';
import 'package:kitchen_mart/bottombar/homebottombar.dart';

class homepage extends StatefulWidget {

  homepage({super.key});

  @override
  State<homepage> createState() => _homepageState();
}

class _homepageState extends State<homepage> {
  final List<String> bannerImages = [
    'assets/banner.png',
    'assets/banner2.png',
    'assets/banner3.png',
  ];

  String? userSelected;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: [
          const homeappbar(),
          Container(
            padding: const EdgeInsets.only(top: 2),
            decoration: const BoxDecoration(
              color: Color(0xFFF8F3EC),
            ),
            child: Column(
              children: [
                Row(
                    children: [
                      Container(
                        margin: const EdgeInsets.symmetric(horizontal: 15),
                        padding: const EdgeInsets.symmetric(horizontal: 15),
                        height: 50,
                        width: 370,
                        child: TypeAheadField(
                          noItemsFoundBuilder: (context) => SizedBox(
                            height: 50,
                            child: Center(
                              child: Text('No Item Found'),
                            ),
                          ),
                          suggestionsBoxDecoration:
                          const SuggestionsBoxDecoration(
                            color: Color(0xFFF8F3EC),
                            elevation: 4.0,
                            borderRadius: BorderRadius.all(
                              Radius.circular(15.0),
                            )
                          ),
                          debounceDuration: const Duration(milliseconds: 400),
                          textFieldConfiguration: TextFieldConfiguration(
                            decoration: InputDecoration(
                              focusedBorder: const OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Color(0xFF34312F),
                                ),
                              ),
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(15.0),
                                ),
                              enabledBorder: const OutlineInputBorder(
                                borderRadius: BorderRadius.all(
                                  Radius.circular(15.0),
                                ),
                                borderSide: BorderSide(
                                  color: Color(0xFF34312F)
                                ),
                              ),
                              hintText: "Search",
                              contentPadding:
                                const EdgeInsets.only(top: 4, left: 10),
                              hintStyle:
                                const TextStyle(
                                  color: Color(0xFF34312F),
                                  fontSize: 14,
                                ),
                              suffixIcon: IconButton(
                                onPressed: () {},
                                icon: const Icon(
                                  Icons.search,
                                color: Color(0xFF34312F),),
                              ),
                              fillColor: Color(0xFFF8F3EC),
                              filled: true
                            )
                          ),
                          suggestionsCallback: (value){
                            return StateService.getSuggestions(value);
                          },
                          itemBuilder: (context, String suggestion) {
                            return ListTile(
                              title: Text(suggestion),
                            );
                          },
                          onSuggestionSelected: (String suggestion) {
                            setState(() {
                              userSelected = suggestion;
                            });
                            Navigator.push(context, MaterialPageRoute(builder: (context) {
                              if (suggestion == 'Cutting Board Organizer') {
                                return bdeal1();
                              }
                              else if (suggestion == 'Toaster') {
                                return bdeal2();
                              }
                              else if (suggestion == 'Rice Cooker') {
                                return bdeal3();
                              }
                              else if (suggestion == 'Colander') {
                                return bdeal4();
                              }
                              else if (suggestion == 'Grater') {
                                return bdeal5();
                              }
                              else if (suggestion == 'Measuring Cups and Spoon') {
                                return bdeal6();
                              }
                              else if (suggestion == 'Food Processor') {
                                return Items1();
                              }
                              else if (suggestion == 'Cutting Board') {
                                return Items2();
                              }
                              else if (suggestion == 'Chef"s Knife') {
                                return Items3();
                              }
                              else if (suggestion == 'Rolling Pin') {
                                return Items4();
                              }
                              else if (suggestion == 'Whisk') {
                                return Items5();
                              }
                              else if (suggestion == 'Potato Masher') {
                                return Items6();
                              }
                              else if (suggestion == 'Garlic Press') {
                                return Items7();
                              }
                              else if (suggestion == 'Coffee Maker') {
                                return apl1();
                              }
                              else if (suggestion == 'Coffee Grinder') {
                                return apl2();
                              }
                              else if (suggestion == 'Deep Fryer') {
                                return apl3();
                              }
                              else if (suggestion == 'Electric Food Scale') {
                                return apl4();
                              }
                              else if (suggestion == 'Dishwasher') {
                                return apl5();
                              }
                              else if (suggestion == 'Electric Bread Slicer') {
                                return apl6();
                              }
                              else if (suggestion == 'Electric Kettle') {
                                return apl7();
                              }
                              else if (suggestion == 'Juicer') {
                                return apl8();
                              }
                              else if (suggestion == 'Microwave Oven') {
                                return apl9();
                              }
                              else if (suggestion == 'Mini Freezer') {
                                return apl10();
                              }
                              else if (suggestion == 'Waffle Maker') {
                                return apl11();
                              }
                              else if (suggestion == 'Basters') {
                                return st1();
                              }
                              else if (suggestion == 'Can Opener') {
                                return st2();
                              }
                              else if (suggestion == 'Food Mills') {
                                return st3();
                              }
                              else if (suggestion == 'Ice Cream Scoop') {
                                return st4();
                              }
                              else if (suggestion == 'Lemon Zester') {
                                return st5();
                              }
                              else if (suggestion == 'Mandoline Slicer') {
                                return st6();
                              }
                              else if (suggestion == 'Mortar & Pestle Set') {
                                return st7();
                              }
                              else if (suggestion == 'Pizza Cutter') {
                                return st8();
                              }
                              else if (suggestion == 'Baking Sheet') {
                                return bt1();
                              }
                              else if (suggestion == 'Bowl Scraper') {
                                return bt2();
                              }
                              else if (suggestion == 'Bundt Pan') {
                                return bt3();
                              }
                              else if (suggestion == 'Cake Pans') {
                                return bt4();
                              }
                              else if (suggestion == 'Measuring Cup') {
                                return bt5();
                              }
                              else if (suggestion == 'Muffin Pan') {
                                return bt6();
                              }
                              else if (suggestion == 'Pastry Brush') {
                                return bt7();
                              }
                              else if (suggestion == 'Pastry Cutter') {
                                return bt8();
                              }
                              else if (suggestion == 'Pizza Pan') {
                                return bt9();
                              }
                              else if (suggestion == 'Wire Rack') {
                                return bt10();
                              }
                              else if (suggestion == 'Apron') {
                                return ct1();
                              }
                              else if (suggestion == 'Colander') {
                                return ct2();
                              }
                              else if (suggestion == 'Frying Pan') {
                                return ct3();
                              }
                              else if (suggestion == 'Ladle') {
                                return ct4();
                              }
                              else if (suggestion == 'Slotted Spoon') {
                                return ct5();
                              }
                              else if (suggestion == 'Spatula') {
                                return ct6();
                              }
                              else if (suggestion == 'Steamer') {
                                return ct7();
                              }
                              else if (suggestion == 'Tongs') {
                                return ct8();
                              }
                              else if (suggestion == 'Wok') {
                                return ct9();
                              }
                              else if (suggestion == 'Wooden Spoon') {
                                return ct10();
                              }
                              else if (suggestion == 'Apple Slicer') {
                                return cc1();
                              }
                              else if (suggestion == 'Biscuit Cutter') {
                                return cc2();
                              }
                              else if (suggestion == 'Box Grater') {
                                return cc3();
                              }
                              else if (suggestion == 'Dough Cutter') {
                                return cc4();
                              }
                              else if (suggestion == 'Kitchen Shears') {
                                return cc5();
                              }
                              else if (suggestion == 'Paring Knife') {
                                return cc6();
                              }
                              else if (suggestion == 'Peeler') {
                                return cc7();
                              }
                              else if (suggestion == 'Steak Hammer') {
                                return cc8();
                              }
                              else if (suggestion == 'Food Storage Container') {
                                return mp1();
                              }
                              else if (suggestion == 'Mixing Bowls') {
                                return mp2();
                              }
                              else if (suggestion == 'Mixing Bowls with Lids') {
                                return mp3();
                              }
                              else if (suggestion == 'Oven Gloves') {
                                return mp4();
                              }
                              else if (suggestion == 'Pastry Blender') {
                                return mp5();
                              }
                              else if (suggestion == 'Rotary Beater') {
                                return mp6();
                              }
                              else if (suggestion == 'Sifter') {
                                return mp7();
                              }
                              else {
                                return homepage();
                              }
                            }));
                          },
                        ),
                      ),
                      Visibility(
                        visible: userSelected == null,
                        child: Text(
                          userSelected ?? '',
                          style: const TextStyle(
                            fontSize: 20,
                          ),
                          ),
                         ),
                         ],
                        ),
                      ]
                    ),
                  ),
                CarouselSlider(
                  options: CarouselOptions(
                    height: 200,
                    autoPlay: true,
                    autoPlayInterval: const Duration(seconds: 2),
                    enlargeCenterPage: true,
                    aspectRatio: 16 / 9,
                  ),
                  items: bannerImages.map((imagePath) {
                    return Container(
                      alignment: Alignment.center,
                      padding: const EdgeInsets.only(top: 5, bottom: 5),
                      margin: const EdgeInsets.symmetric(horizontal: 2),
                      child: Image.asset(imagePath),
                    );
                  }).toList(),
                ),
                Container(
                  alignment: Alignment.centerLeft,
                  margin: const EdgeInsets.only(top: 15, left: 10),
                  child: const Text(
                    "Best Deals",
                    style: TextStyle(
                      fontSize: 25,
                      fontWeight: FontWeight.bold,
                      color: Color(0xFF34312F),
                    ),
                  ),
                ),
                DealsWidget(),
                const SizedBox(height: 10),
                Container(
                  alignment: Alignment.centerLeft,
                  margin: const EdgeInsets.only(left: 10),
                  child: const Text(
                    "Categories",
                    style: TextStyle(
                      fontSize: 25,
                      fontWeight: FontWeight.bold,
                      color: Color(0xFF34312F),
                    ),
                  ),
                ),
                ItemWidget(),
                Container(
                  alignment: Alignment.centerLeft,
                  margin: const EdgeInsets.only(top: 15, left: 10),
                  child: const Text(
                    "P199 & Below",
                    style: TextStyle(
                      fontSize: 25,
                      fontWeight: FontWeight.bold,
                      color: Color(0xFF34312F),
                    ),
                  ),
                ),
                SaleWidget(),
                const SizedBox(height: 10),
              ],
            ),
      bottomNavigationBar: const homebottombar(),
    );
  }
}
