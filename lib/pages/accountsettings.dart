import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class accountsettings extends StatefulWidget {
  final String initialName;

  accountsettings({required this.initialName});

  @override
  _AccountSettingsPageState createState() => _AccountSettingsPageState();
}

class _AccountSettingsPageState extends State<accountsettings> {
  TextEditingController name = TextEditingController();
  TextEditingController address = TextEditingController();
  TextEditingController contactnumber = TextEditingController();


  Future<void> insertrecord() async {
    if (name.text != "" && address.text != "" && contactnumber.text != "") {

      try {
        String uri = "http://192.168.100.5/userinfo/insert_record.php";

        var res = await http.post(Uri.parse(uri), body: {
          "name": name.text,
          "address": address.text,
          "contactnumber": contactnumber.text,

        });

        var response = jsonDecode(res.body);
        if (response ["success"] == "true") {
          print("Data Inserted");
        }
        else {
          print("some issue");
        }
      }
      catch (e) {
        print(e);
      }
    }
    else {
      print("Please Fill All Fields");
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xFFB28264),
        title: Text(
          "Account Settings",
          style: TextStyle(
            color: Color(0xFFF8F3EC),
          ),
        ),
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios_rounded,
            color: Color(0xFFF8F3EC),
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          children: [
            TextField(
              controller: name,
              decoration: InputDecoration(labelText: 'Name'),
            ),
            TextField(
              controller: address,
              decoration: InputDecoration(labelText: 'Address'),
            ),
            TextField(
              controller: contactnumber,
              decoration: InputDecoration(labelText: 'Contact Number'),
            ),
            SizedBox(height: 16),
            ElevatedButton(
              onPressed: () {
                insertrecord();
              },
              style: ButtonStyle(
                minimumSize: MaterialStateProperty.all(const Size(100.0, 40.0)),
                backgroundColor: MaterialStateProperty.all(
                  const Color(0xFFFACC5F),
                ),
                padding: MaterialStateProperty.all(
                  const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                ),
                shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                  RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20),
                  ),
                ),
              ),
              child: Text('Save Settings'),
            )
          ],
        ),
      ),
    );
  }
}
