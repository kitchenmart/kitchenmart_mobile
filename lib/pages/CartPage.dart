import 'package:flutter/material.dart';
import 'package:kitchen_mart/CartItem.dart';
import 'package:kitchen_mart/pages/CheckoutPage.dart';

class CartPage extends StatefulWidget {
  final List<CartItem> cartItems;

  CartPage({required this.cartItems});

  @override
  _CartPageState createState() => _CartPageState();
}

class _CartPageState extends State<CartPage> {
  List<bool> selectedItems = [];

  @override
  void initState() {
    super.initState();

    selectedItems = List<bool>.generate(widget.cartItems.length, (index) => false);
  }

  void _deleteSelectedItems() {

    final List<int> indicesToRemove = [];
    for (int i = 0; i < selectedItems.length; i++) {
      if (selectedItems[i]) {
        indicesToRemove.add(i);
      }
    }


    for (int i = indicesToRemove.length - 1; i >= 0; i--) {
      final int indexToRemove = indicesToRemove[i];
      setState(() {
        widget.cartItems.removeAt(indexToRemove);
        selectedItems.removeAt(indexToRemove);
      });
    }
  }

  Widget _customCheckbox(bool value, int index) {
    return InkWell(
      onTap: () {
        setState(() {
          selectedItems[index] = !selectedItems[index];
        });
      },
      child: Container(
        width: 24.0,
        height: 24.0,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          border: Border.all(
            color: Color(0xFFB28264),
          ),
        ),
        child: value
            ? Icon(
          Icons.check,
          size: 20.0,
          color: Color(0xFFB28264),
        )
            : null,
      ),
    );
  }

  double getTotalPrice() {
    double totalPrice = 0.0;
    for (int i = 0; i < widget.cartItems.length; i++) {
      final cartItem = widget.cartItems[i];
      totalPrice += cartItem.productPrice * cartItem.quantity;
    }
    return totalPrice;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xFFB28264),
        title: Text(
          "My Cart",
          style: TextStyle(
            color: Color(0xFFF8F3EC),
          ),
        ),
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios_rounded,
            color: Color(0xFFF8F3EC),
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        actions: [
          IconButton(
            icon: Icon(
              Icons.delete,
              color: Color(0xFFF8F3EC),
            ),
            onPressed: () {
              _deleteSelectedItems();
            },
          ),
        ],
      ),
      body: Container(
        color: Color(0xFF8F3EC),
        child: ListView.builder(
          itemCount: widget.cartItems.length,
          itemBuilder: (context, index) {
            final cartItem = widget.cartItems[index];
            return Card(
              shadowColor: Color(0xFFB28264),
              child: ListTile(
                leading: Image.asset(cartItem.productImage),
                title: Text(cartItem.productName),
                subtitle: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Price: \P ${cartItem.productPrice.toStringAsFixed(2)}",
                      style: TextStyle(
                        color: Colors.black,
                      ),
                    ),
                    Text(
                      "Quantity: ${cartItem.quantity}",
                      style: TextStyle(
                        color: Colors.black,
                      ),
                    ),
                  ],
                ),
                trailing: _customCheckbox(selectedItems[index], index),
              ),
            );
          },
        ),
      ),
      bottomNavigationBar: Container(
        height: 100,
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
        decoration: BoxDecoration(
          color: const Color(0xFF34312F),
        ),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "Total Price: \P ${getTotalPrice().toStringAsFixed(2)}",
                  style: TextStyle(
                    color: Color(0xFFF8F3EC),
                    fontSize: 18,
                  ),
                ),
                ElevatedButton(
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => CheckoutPage(cartItems: widget.cartItems, totalPrice: getTotalPrice()),
                      ),
                    );
                  },
                  style: ButtonStyle(
                    minimumSize: MaterialStateProperty.all(const Size(100.0, 40.0)),
                    backgroundColor: MaterialStateProperty.all(
                      const Color(0xFFFACC5F),
                    ),
                    padding: MaterialStateProperty.all(
                      const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                    ),
                    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                      RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20),
                      ),
                    ),
                  ),
                  child: Text(
                    "Checkout",
                    style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
