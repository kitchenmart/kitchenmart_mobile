import 'package:flutter/material.dart';
import '../CartItem.dart';

class CheckoutPage extends StatefulWidget {
  final List<CartItem> cartItems;
  final double totalPrice;

  CheckoutPage({required this.cartItems, required this.totalPrice});

  @override
  _CheckoutPageState createState() => _CheckoutPageState();
}

class _CheckoutPageState extends State<CheckoutPage> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  String? name;
  String? contactNumber;
  String? address;
  String? paymentMethod = "Cash On Delivery";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xFFB28264),
        title: Text(
          "Checkout",
          style: TextStyle(
            color: Color(0xFFF8F3EC),
          ),
        ),
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios_rounded,
            color: Color(0xFFF8F3EC),
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Form(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Order Summary",
                  style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                SizedBox(height: 16),
                Text("Total Price: \P ${widget.totalPrice.toStringAsFixed(2)}"),
                SizedBox(height: 16),

                // Payment Method Text
                Text(
                  "Payment Method",
                  style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                SizedBox(height: 16),

                // Payment Method Radio Buttons
                ListTile(
                  title: Text("Cash On Delivery"),
                  leading: Radio(
                    value: "Cash On Delivery",
                    groupValue: paymentMethod,
                    onChanged: (value) {
                      setState(() {
                        paymentMethod = value as String?;
                      });
                    },
                  ),
                ),

                SizedBox(height: 16),

                // Name
                TextFormField(
                  decoration: InputDecoration(labelText: "Name"),
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Name is required';
                    }
                    return null;
                  },
                  onChanged: (value) {
                    setState(() {
                      name = value;
                    });
                  },
                ),
                SizedBox(height: 16),

                // Contact Number
                TextFormField(
                  decoration: InputDecoration(labelText: "Contact Number"),
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Contact Number is required';
                    }
                    return null;
                  },
                  onChanged: (value) {
                    setState(() {
                      contactNumber = value;
                    });
                  },
                ),
                SizedBox(height: 16),

                // Address
                TextFormField(
                  decoration: InputDecoration(labelText: "Address"),
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Address is required';
                    }
                    return null;
                  },
                  onChanged: (value) {
                    setState(() {
                      address = value;
                    });
                  },
                ),
                SizedBox(height: 16),

                Text("Please prepare \P ${widget.totalPrice.toStringAsFixed(2)}"),
                SizedBox(height: 16),

                ElevatedButton(
                  onPressed: () {
                    // Check if the form is valid
                    if (_formKey.currentState!.validate()) {
                      showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return AlertDialog(
                            backgroundColor: Color(0xFF34312F),
                            content: Text(
                              "Ordered Successfully",
                              style: TextStyle(
                                color: Color(0xFFF8F3EC),
                              ),
                            ),
                            actions: <Widget>[
                              TextButton(
                                child: Text(
                                  "OK",
                                  style: TextStyle(
                                    color: Color(0xFFF8F3EC),
                                  ),
                                ),
                                onPressed: () {
                                  // Close the dialog
                                  Navigator.of(context).pop();

                                  // Navigate back to the homepage
                                  Navigator.of(context).pop();
                                },
                              ),
                            ],
                          );
                        },
                      );
                    }
                  },
                  style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all(
                      const Color(0xFFFACC5F),
                    ),
                    padding: MaterialStateProperty.all(
                      const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                    ),
                  ),
                  child: Text(
                    "ORDER",
                    style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
