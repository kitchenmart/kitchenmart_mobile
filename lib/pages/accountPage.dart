import 'package:flutter/material.dart';
import 'package:kitchen_mart/appbar/accountapp.dart';
import 'package:kitchen_mart/bottombar/accountbottom.dart';
import 'package:kitchen_mart/pages/homepage.dart';

class accountPage extends StatelessWidget {
  const accountPage({super.key}); 

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: [
          const accountapp(),
          Container(
            padding: const EdgeInsets.only(top: 2),
            decoration: const BoxDecoration(
            color: Color(0xFFF8F3EC),
            ),
          ),
          const SizedBox(height: 5),

          Container(
            padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
            decoration: const BoxDecoration(
                color: Color(0xFFFFFFFF),
            ),
            alignment: Alignment.centerLeft,
            margin: const EdgeInsets.only(top: 10),
            child: const Text(
              "My Orders",
              style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.w700,
                color: Color(0xFF34312F),
              ),
            ),
          ),
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
            decoration: const BoxDecoration(
                color: Color(0xFFFFFFFF),
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(25),
                  bottomRight: Radius.circular(25),
                )
            ),
          child: const Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(width: 10),
                Icon(
                  Icons.local_shipping,
                  color: Color(0xFFB28264),
                  size: 40,
                ),
                SizedBox(width: 5),
                Text(
                  "To Receive",
                  style: TextStyle(
                      fontSize: 17,
                      color: Color(0xFF34312F)
                  ),
                ),
                SizedBox(width: 40),
                Icon(
                  Icons.message,
                  color: Color(0xFFB28264),
                  size: 40,
                ),
                SizedBox(width: 5),
                Text(
                  "To Review",
                  style: TextStyle(
                      fontSize: 17,
                      color: Color(0xFF34312F)
                  ),
                ),
            ]
          ),
          ),
          SizedBox(height: 20),
          InkWell(
          onTap: () {
            Navigator.pushNamed(context, "accountsettings");
          },
          child: Container(
            padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
            decoration: const BoxDecoration(
              color: Color(0xFFFFFFFF),
              borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(25),
                bottomRight: Radius.circular(25),
                topRight: Radius.circular(25),
                topLeft: Radius.circular(25),
              )
          ),
          child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                SizedBox(width: 10),
                Icon(
                  Icons.person,
                  color: Color(0xFFB28264),
                  size: 40,
                ),
                SizedBox(width: 5),
                Text(
                  "Account Settings",
                  style: TextStyle(
                      fontSize: 17,
                      color: Color(0xFF34312F)
                  ),
                ),
              ]
          ),
          ),
          ),
          SizedBox(height: 20),
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
            decoration: const BoxDecoration(
                color: Color(0xFFFFFFFF),
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(25),
                  bottomRight: Radius.circular(25),
                  topRight: Radius.circular(25),
                  topLeft: Radius.circular(25),
                )
            ),
            child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  SizedBox(width: 10),
                  Icon(
                    Icons.email_rounded,
                    color: Color(0xFFB28264),
                    size: 40,
                  ),
                  SizedBox(width: 10),
                  Text(
                    "Message KitchenMart",
                    style: TextStyle(
                        fontSize: 17,
                        color: Color(0xFF34312F)
                    ),
                  ),
                ]
            ),
          ),
      ]
    ),
    bottomNavigationBar: const accountbottom(),
        );
      }
    }