import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:kitchen_mart/Bdeal_itempage/bdeal1.dart';
import 'package:kitchen_mart/Bdeal_itempage/bdeal2.dart';
import 'package:kitchen_mart/Bdeal_itempage/bdeal3.dart';
import 'package:kitchen_mart/Bdeal_itempage/bdeal4.dart';
import 'package:kitchen_mart/Bdeal_itempage/bdeal5.dart';
import 'package:kitchen_mart/Bdeal_itempage/bdeal6.dart';
import 'package:kitchen_mart/Category/Category1.dart';
import 'package:kitchen_mart/Category/Category2.dart';
import 'package:kitchen_mart/Category/Category3.dart';
import 'package:kitchen_mart/Category/Category4.dart';
import 'package:kitchen_mart/Category/Category5.dart';
import 'package:kitchen_mart/Category/Category6.dart';
import 'package:kitchen_mart/Specialty_itempage/st1.dart';
import 'package:kitchen_mart/Specialty_itempage/st2.dart';
import 'package:kitchen_mart/Specialty_itempage/st3.dart';
import 'package:kitchen_mart/Specialty_itempage/st4.dart';
import 'package:kitchen_mart/Specialty_itempage/st5.dart';
import 'package:kitchen_mart/Specialty_itempage/st6.dart';
import 'package:kitchen_mart/Specialty_itempage/st7.dart';
import 'package:kitchen_mart/Specialty_itempage/st8.dart';
import 'package:kitchen_mart/appliances_itempage/apl1.dart';
import 'package:kitchen_mart/appliances_itempage/apl10.dart';
import 'package:kitchen_mart/appliances_itempage/apl11.dart';
import 'package:kitchen_mart/appliances_itempage/apl2.dart';
import 'package:kitchen_mart/appliances_itempage/apl3.dart';
import 'package:kitchen_mart/appliances_itempage/apl4.dart';
import 'package:kitchen_mart/appliances_itempage/apl5.dart';
import 'package:kitchen_mart/appliances_itempage/apl6.dart';
import 'package:kitchen_mart/appliances_itempage/apl7.dart';
import 'package:kitchen_mart/appliances_itempage/apl8.dart';
import 'package:kitchen_mart/appliances_itempage/apl9.dart';
import 'package:kitchen_mart/baking_itempage/bt1.dart';
import 'package:kitchen_mart/baking_itempage/bt10.dart';
import 'package:kitchen_mart/baking_itempage/bt2.dart';
import 'package:kitchen_mart/baking_itempage/bt3.dart';
import 'package:kitchen_mart/baking_itempage/bt4.dart';
import 'package:kitchen_mart/baking_itempage/bt5.dart';
import 'package:kitchen_mart/baking_itempage/bt6.dart';
import 'package:kitchen_mart/baking_itempage/bt7.dart';
import 'package:kitchen_mart/baking_itempage/bt8.dart';
import 'package:kitchen_mart/baking_itempage/bt9.dart';
import 'package:kitchen_mart/below_itempage/Items1.dart';
import 'package:kitchen_mart/below_itempage/Items2.dart';
import 'package:kitchen_mart/below_itempage/Items3.dart';
import 'package:kitchen_mart/below_itempage/Items4.dart';
import 'package:kitchen_mart/below_itempage/Items5.dart';
import 'package:kitchen_mart/below_itempage/Items6.dart';
import 'package:kitchen_mart/below_itempage/Items7.dart';
import 'package:kitchen_mart/cooking_itempage/ct1.dart';
import 'package:kitchen_mart/cooking_itempage/ct10.dart';
import 'package:kitchen_mart/cooking_itempage/ct2.dart';
import 'package:kitchen_mart/cooking_itempage/ct3.dart';
import 'package:kitchen_mart/cooking_itempage/ct4.dart';
import 'package:kitchen_mart/cooking_itempage/ct5.dart';
import 'package:kitchen_mart/cooking_itempage/ct6.dart';
import 'package:kitchen_mart/cooking_itempage/ct7.dart';
import 'package:kitchen_mart/cooking_itempage/ct8.dart';
import 'package:kitchen_mart/cooking_itempage/ct9.dart';
import 'package:kitchen_mart/cutting_itempage/cc1.dart';
import 'package:kitchen_mart/cutting_itempage/cc2.dart';
import 'package:kitchen_mart/cutting_itempage/cc3.dart';
import 'package:kitchen_mart/cutting_itempage/cc4.dart';
import 'package:kitchen_mart/cutting_itempage/cc5.dart';
import 'package:kitchen_mart/cutting_itempage/cc6.dart';
import 'package:kitchen_mart/cutting_itempage/cc7.dart';
import 'package:kitchen_mart/cutting_itempage/cc8.dart';
import 'package:kitchen_mart/mixing_itempage/mp1.dart';
import 'package:kitchen_mart/mixing_itempage/mp2.dart';
import 'package:kitchen_mart/mixing_itempage/mp3.dart';
import 'package:kitchen_mart/mixing_itempage/mp4.dart';
import 'package:kitchen_mart/mixing_itempage/mp5.dart';
import 'package:kitchen_mart/mixing_itempage/mp6.dart';
import 'package:kitchen_mart/mixing_itempage/mp7.dart';
import 'package:kitchen_mart/pages/LoginPage.dart';
import 'package:kitchen_mart/pages/SignupPage.dart';
import 'package:kitchen_mart/pages/accountPage.dart';
import 'package:kitchen_mart/pages/accountsettings.dart';
import 'package:kitchen_mart/pages/homepage.dart';

void main() {
  runApp(
      const MyApp(),
  );
}


class MyApp extends StatelessWidget{
  const MyApp({super.key});

  @override
  Widget build(BuildContext context){
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          scaffoldBackgroundColor: const Color(0xFFF8F3EC),
        ),
        routes: {
          "/" : (context) => const splashscreen(),
          "homepage" : (context) => homepage(),
          "accountsettings" : (context) => accountsettings(initialName: '',),
          "accountPage" : (context) => const accountPage(),
          "LoginPage" : (context) => const LoginPage(),
          "SignupPage" : (context) => const SignupPage(),
          "Category1" : (context) => Category1(),
          "Category2" : (context) => Category2(),
          "Category3" : (context) => Category3(),
          "Category4" : (context) => Category4(),
          "Category5" : (context) => Category5(),
          "Category6" : (context) => Category6(),
          "Items1" : (context) => Items1(),
          "Items2" : (context) => Items2(),
          "Items3" : (context) => Items3(),
          "Items4" : (context) => Items4(),
          "Items5" : (context) => Items5(),
          "Items6" : (context) => Items6(),
          "Items7" : (context) => Items7(),
          "bdeal1" : (context) => bdeal1(),
          "bdeal2" : (context) => bdeal2(),
          "bdeal3" : (context) => bdeal3(),
          "bdeal4" : (context) => bdeal4(),
          "bdeal5" : (context) => bdeal5(),
          "bdeal6" : (context) => bdeal6(),
          "apl1" : (context) => apl1(),
          "apl2" : (context) => apl2(),
          "apl3" : (context) => apl3(),
          "apl4" : (context) => apl4(),
          "apl5" : (context) => apl5(),
          "apl6" : (context) => apl6(),
          "apl7" : (context) => apl7(),
          "apl8" : (context) => apl8(),
          "apl9" : (context) => apl9(),
          "apl10" : (context) => apl10(),
          "apl11" : (context) => apl11(),
          "st1" : (context) => st1(),
          "st2" : (context) => st2(),
          "st3" : (context) => st3(),
          "st4" : (context) => st4(),
          "st5" : (context) => st5(),
          "st6" : (context) => st6(),
          "st7" : (context) => st7(),
          "st8" : (context) => st8(),
          "bt1" : (context) => bt1(),
          "bt2" : (context) => bt2(),
          "bt3" : (context) => bt3(),
          "bt4" : (context) => bt4(),
          "bt5" : (context) => bt5(),
          "bt6" : (context) => bt6(),
          "bt7" : (context) => bt7(),
          "bt8" : (context) => bt8(),
          "bt9" : (context) => bt9(),
          "bt10" : (context) => bt10(),
          "ct1" : (context) => ct1(),
          "ct2" : (context) => ct2(),
          "ct3" : (context) => ct3(),
          "ct4" : (context) => ct4(),
          "ct5" : (context) => ct5(),
          "ct6" : (context) => ct6(),
          "ct7" : (context) => ct7(),
          "ct8" : (context) => ct8(),
          "ct9" : (context) => ct9(),
          "ct10" : (context) => ct10(),
          "cc1" : (context) => cc1(),
          "cc2" : (context) => cc2(),
          "cc3" : (context) => cc3(),
          "cc4" : (context) => cc4(),
          "cc5" : (context) => cc5(),
          "cc6" : (context) => cc6(),
          "cc7" : (context) => cc7(),
          "cc8" : (context) => cc8(),
          "mp1" : (context) => mp1(),
          "mp2" : (context) => mp2(),
          "mp3" : (context) => mp3(),
          "mp4" : (context) => mp4(),
          "mp5" : (context) => mp5(),
          "mp6" : (context) => mp6(),
          "mp7" : (context) => mp7(),

        }
    );
  }
  }
class splashscreen extends StatefulWidget {
  const splashscreen ({Key? key}) : super(key: key);

  @override
  State<splashscreen> createState() => _splashscreenState();
}
class _splashscreenState extends State<splashscreen> {

  @override
  void initState() {
    super.initState();
    Future.delayed(const Duration(seconds: 2)).then((value){
      Navigator.of(context).pushReplacement(
          CupertinoPageRoute(builder: (ctx) => homepage()));
    });
  }

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      backgroundColor: Color(0xFFF8F3EC),
      body: SizedBox(
        width: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image(
              image: AssetImage('assets/logo.png'),
              width: 300,
            ),
            SizedBox(height: 50),
            SpinKitWaveSpinner(
              color: Color(0xFFB28264),
              size: 50.0,
            ),
          ],
        ),
      ),
    );
  }
}

