class CartItem {
  final String productName;
  final String productImage;
  final double productPrice;
  int quantity;

  CartItem({
    required this.productName,
    required this.productImage,
    required this.productPrice,
    this.quantity = 1,
  });
}




