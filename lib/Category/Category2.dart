import 'package:flutter/material.dart';

class Product {
  final String imageUrl;
  final String productName;
  final String price;

  Product({
  required this.imageUrl,
  required this.productName,
  required this.price,
});
}

class Category2 extends StatefulWidget {
  final List<Product> products = [
    Product(
      imageUrl: 'assets/specialty/st1.png',
      productName: 'Basters',
      price: '\P 259',
    ),
    Product(
      imageUrl: 'assets/specialty/st2.jpg',
      productName: 'Can Opener',
      price: '\P 250',
    ),
    Product(
      imageUrl: 'assets/specialty/st3.png',
      productName: 'Food Mills',
      price: '\P 490',
    ),
    Product(
      imageUrl: 'assets/specialty/st4.png',
      productName: 'Ice Cream Scoop',
      price: '\P 229',
    ),
    Product(
      imageUrl: 'assets/specialty/st5.jpg',
      productName: 'Lemon Zester',
      price: '\P 240',
    ),
    Product(
      imageUrl: 'assets/specialty/st6.jpg',
      productName: 'Mandoline Slicer',
      price: '\P 310',
    ),
    Product(
      imageUrl: 'assets/specialty/st7.png',
      productName: 'Mortar & Pestle Set',
      price: '\P 359',
    ),
    Product(
      imageUrl: 'assets/specialty/st8.png',
      productName: 'Pizza Cutter',
      price: '\P 280',
    ),
  ];

  @override
  _Category2State createState() => _Category2State();
}

class _Category2State extends State<Category2> {
  List<bool> isLiked = List.generate(8, (index) => false);

  void showSnackBar(String text) {
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text(text),
        duration: Duration(seconds: 1),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xFFB28264),
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios_rounded),
          color: Color(0xFFF8F3EC),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        title: Text(
          "Specialty Tools",
          style: TextStyle(
            color: Color(0xFFF8F3EC),
          ),
        ),
      ),
      body: Container(
        color: Color(0xFFF8F3EC),
        child: GridView.builder(
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2,
            crossAxisSpacing: 10.0,
            mainAxisSpacing: 10.0,
          ),
          itemCount: widget.products.length,
          itemBuilder: (context, index) {
            return _buildProductBox(
              imageUrl: widget.products[index].imageUrl,
              productName: widget.products[index].productName,
              price: widget.products[index].price,
              isLiked: isLiked[index],
              onTapImage: () {
                if (index == 0) {
                  Navigator.pushNamed(context, 'st1');
                } else if (index == 1) {
                  Navigator.pushNamed(context, 'st2');
                } else if (index == 2) {
                  Navigator.pushNamed(context, 'st3');
                } else if (index == 3) {
                  Navigator.pushNamed(context, 'st4');
                } else if (index == 4) {
                  Navigator.pushNamed(context, 'st5');
                } else if (index == 5) {
                  Navigator.pushNamed(context, 'st6');
                } else if (index == 6) {
                  Navigator.pushNamed(context, 'st7');
                } else if (index == 7) {
                  Navigator.pushNamed(context, 'st8');
                }
              },
              onTapFavorite: () {
                setState(() {
                  isLiked[index] = !isLiked[index];
                  showSnackBar(isLiked[index]
                      ? 'You liked ${widget.products[index].productName}'
                      : 'You unliked ${widget.products[index].productName}');
                });
              },
            );
          },
        ),
      ),
    );
  }

  Widget _buildProductBox({
    required String imageUrl,
    required String productName,
    required String price,
    required bool isLiked,
    required VoidCallback onTapImage,
    required VoidCallback onTapFavorite,
  }) {
    return GestureDetector(
      onTap: onTapImage,
      child: Container(
        padding: EdgeInsets.all(16.0),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10.0),
          boxShadow: [
            BoxShadow(
              color: Color(0xFFB28264),
              blurRadius: 5.0,
              offset: Offset(0, 2),
            ),
          ],
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 6.0),
            Center(
              child: Image.asset(
                imageUrl,
                width: 100,
                height: 100,
              ),
            ),
            SizedBox(height: 6.0),
            Text(
              productName,
              style: TextStyle(
                fontSize: 15.0,
                fontWeight: FontWeight.bold,
                color: Color(0xFFB28264),
              ),
            ),
            SizedBox(height: 1.0),
            Expanded(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    price,
                    style: TextStyle(fontSize: 16.0),
                  ),
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5),
                      color: Color(0xFF34312F),
                    ),
                    child: IconButton(
                      icon: Icon(
                        isLiked
                            ? Icons.favorite_rounded
                            : Icons.favorite_border,
                      ),
                      color: Color(0xFFFACC5F),
                      onPressed: onTapFavorite,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

