import 'package:flutter/material.dart';

class Product {
  final String imageUrl;
  final String productName;
  final String price;

  Product({
    required this.imageUrl,
    required this.productName,
    required this.price,
  });
}

class Category1 extends StatefulWidget {
  final List<Product> products = [
    Product(
      imageUrl: 'assets/appliances/ap1.webp',
      productName: 'Coffee Maker',
      price: '\P 5900',
    ),
    Product(
      imageUrl: 'assets/appliances/ap2.png',
      productName: 'Coffee Grinder',
      price: '\P 6799',
    ),
    Product(
      imageUrl: 'assets/appliances/ap3.png',
      productName: 'Deep Fryer',
      price: '\P 6999',
    ),
    Product(
      imageUrl: 'assets/appliances/ap4.webp',
      productName: 'Electric Food Scale',
      price: '\P 479',
    ),
    Product(
      imageUrl: 'assets/appliances/ap5.png',
      productName: 'Dishwasher',
      price: '\P 12999',
    ),
    Product(
      imageUrl: 'assets/appliances/ap6.webp',
      productName: 'Electric Bread Slicer',
      price: '\P 1705',
    ),
    Product(
      imageUrl: 'assets/appliances/ap7.jpg',
      productName: 'Electric Kettle',
      price: '\P 1223',
    ),
    Product(
      imageUrl: 'assets/appliances/ap8.webp',
      productName: 'Juicer',
      price: '\P 2385',
    ),
    Product(
      imageUrl: 'assets/appliances/ap9.jpg',
      productName: 'Microwave Oven',
      price: '\P 3333',
    ),
    Product(
      imageUrl: 'assets/appliances/ap10.png',
      productName: 'Mini Freezer',
      price: '\P 13999',
    ),
    Product(
      imageUrl: 'assets/appliances/ap11.png',
      productName: 'Waffle Maker',
      price: '\P 1599',
    ),
  ];

  @override
  _Category1State createState() => _Category1State();
}

class _Category1State extends State<Category1> {
  List<bool> isLiked = List.generate(11, (index) => false);

  void showSnackBar(String text) {
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text(text),
        duration: Duration(seconds: 1),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xFFB28264),
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios_rounded),
          color: Color(0xFFF8F3EC),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        title: Text(
          "Appliances",
          style: TextStyle(
            color: Color(0xFFF8F3EC),
          ),
        ),
      ),
      body: Container(
        color: Color(0xFFF8F3EC),
        child: GridView.builder(
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2,
            crossAxisSpacing: 10.0,
            mainAxisSpacing: 10.0,
          ),
          itemCount: widget.products.length,
          itemBuilder: (context, index) {
            return _buildProductBox(
              imageUrl: widget.products[index].imageUrl,
              productName: widget.products[index].productName,
              price: widget.products[index].price,
              isLiked: isLiked[index],
              onTapImage: () {
                if (index == 0) {
                  Navigator.pushNamed(context, 'apl1');
                } else if (index == 1) {
                  Navigator.pushNamed(context, 'apl2');
                } else if (index == 2) {
                  Navigator.pushNamed(context, 'apl3');
                } else if (index == 3) {
                  Navigator.pushNamed(context, 'apl4');
                } else if (index == 4) {
                  Navigator.pushNamed(context, 'apl5');
                } else if (index == 5) {
                  Navigator.pushNamed(context, 'apl6');
                } else if (index == 6) {
                  Navigator.pushNamed(context, 'apl7');
                } else if (index == 7) {
                  Navigator.pushNamed(context, 'apl8');
                } else if (index == 8) {
                  Navigator.pushNamed(context, 'apl9');
                }else if (index == 9) {
                  Navigator.pushNamed(context, 'apl10');
                } else if (index == 10) {
                  Navigator.pushNamed(context, 'apl11');
                }
              },
              onTapFavorite: () {
                setState(() {
                  isLiked[index] = !isLiked[index];
                  showSnackBar(isLiked[index]
                      ? 'You liked ${widget.products[index].productName}'
                      : 'You unliked ${widget.products[index].productName}');
                });
              },
            );
          },
        ),
      ),
    );
  }

  Widget _buildProductBox({
    required String imageUrl,
    required String productName,
    required String price,
    required bool isLiked,
    required VoidCallback onTapImage,
    required VoidCallback onTapFavorite,
  }) {
    return GestureDetector(
      onTap: onTapImage,
      child: Container(
        padding: EdgeInsets.all(16.0),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10.0),
          boxShadow: [
            BoxShadow(
              color: Color(0xFFB28264),
              blurRadius: 5.0,
              offset: Offset(0, 2),
            ),
          ],
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 6.0),
            Center(
              child: Image.asset(
                imageUrl,
                width: 100,
                height: 100,
              ),
            ),
            SizedBox(height: 6.0),
            Text(
              productName,
              style: TextStyle(
                fontSize: 15.0,
                fontWeight: FontWeight.bold,
                color: Color(0xFFB28264),
              ),
            ),
            SizedBox(height: 1.0),
            Expanded(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    price,
                    style: TextStyle(fontSize: 16.0),
                  ),
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5),
                      color: Color(0xFF34312F),
                    ),
                    child: IconButton(
                      icon: Icon(
                        isLiked
                            ? Icons.favorite_rounded
                            : Icons.favorite_border,
                      ),
                      color: Color(0xFFFACC5F),
                      onPressed: onTapFavorite,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
