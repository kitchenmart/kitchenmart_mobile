import 'package:flutter/material.dart';

class Product {
  final String imageUrl;
  final String productName;
  final String price;

  Product({
    required this.imageUrl,
    required this.productName,
    required this.price,
  });
}

class Category5 extends StatefulWidget {
  final List<Product> products = [

    Product(
      imageUrl: 'assets/Cutting/cc1.png',
      productName: 'Apple Slicer',
      price: '\P 250',
    ),
    Product(
      imageUrl: 'assets/Cutting/cc2.png',
      productName: 'Biscuit Cutter',
      price: '\P 399',
    ),
    Product(
      imageUrl: 'assets/Cutting/cc3.png',
      productName: 'Box Grater',
      price: '\P 450',
    ),
    Product(
      imageUrl: 'assets/Cutting/cc4.jpg',
      productName: 'Dough Cutter',
      price: '\P 450',
    ),
    Product(
      imageUrl: 'assets/Cutting/cc5.jpg',
      productName: 'Kitchen Shears',
      price: '\P 340',
    ),
    Product(
      imageUrl: 'assets/Cutting/cc6.jpg',
      productName: 'Paring Knife',
      price: '\P 519',
    ),
    Product(
      imageUrl: 'assets/Cutting/cc7.png',
      productName: 'Peeler',
      price: '\P 299',
    ),
    Product(
      imageUrl: 'assets/Cutting/cc8.png',
      productName: 'Steak Hammer',
      price: '\P 299',
    ),
  ];

  @override
  _Category5State createState() => _Category5State();
}

class _Category5State extends State<Category5> {
  List<bool> isLiked = List.generate(8, (index) => false);

  void showSnackBar(String text) {
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text(text),
        duration: Duration(seconds: 1),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xFFB28264),
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios_rounded),
          color: Color(0xFFF8F3EC),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        title: Text(
          "Cutting And Chopping",
          style: TextStyle(
            color: Color(0xFFF8F3EC),
          ),
        ),
      ),
      body: Container(
        color: Color(0xFFF8F3EC),
        child: GridView.builder(
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2,
            crossAxisSpacing: 10.0,
            mainAxisSpacing: 10.0,
          ),
          itemCount: widget.products.length,
          itemBuilder: (context, index) {
            return _buildProductBox(
              imageUrl: widget.products[index].imageUrl,
              productName: widget.products[index].productName,
              price: widget.products[index].price,
              isLiked: isLiked[index],
              onTapImage: () {
                if (index == 0) {
                  Navigator.pushNamed(context, 'cc1');
                } else if (index == 1) {
                  Navigator.pushNamed(context, 'cc2');
                } else if (index == 2) {
                  Navigator.pushNamed(context, 'cc3');
                } else if (index == 3) {
                  Navigator.pushNamed(context, 'cc4');
                } else if (index == 4) {
                  Navigator.pushNamed(context, 'cc5');
                } else if (index == 5) {
                  Navigator.pushNamed(context, 'cc6');
                } else if (index == 6) {
                  Navigator.pushNamed(context, 'cc7');
                } else if (index == 7) {
                  Navigator.pushNamed(context, 'cc8');
                }
              },
              onTapFavorite: () {
                setState(() {
                  isLiked[index] = !isLiked[index];
                  showSnackBar(isLiked[index]
                      ? 'You liked ${widget.products[index].productName}'
                      : 'You unliked ${widget.products[index].productName}');
                });
              },
            );
          },
        ),
      ),
    );
  }

  Widget _buildProductBox({
    required String imageUrl,
    required String productName,
    required String price,
    required bool isLiked,
    required VoidCallback onTapImage,
    required VoidCallback onTapFavorite,
  }) {
    return GestureDetector(
      onTap: onTapImage,
      child: Container(
        padding: EdgeInsets.all(16.0),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10.0),
          boxShadow: [
            BoxShadow(
              color: Color(0xFFB28264),
              blurRadius: 5.0,
              offset: Offset(0, 2),
            ),
          ],
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 6.0),
            Center(
              child: Image.asset(
                imageUrl,
                width: 100,
                height: 100,
              ),
            ),
            SizedBox(height: 6.0),
            Text(
              productName,
              style: TextStyle(
                  fontSize: 15.0, fontWeight: FontWeight.bold,
                  color: Color(0xFFB28264)),
            ),
            SizedBox(height: 1.0),
            Expanded(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    price,
                    style: TextStyle(fontSize: 16.0),
                  ),
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5),
                      color: Color(0xFF34312F),
                    ),
                    child: IconButton(
                      icon: Icon(
                        isLiked
                            ? Icons.favorite_rounded
                            : Icons.favorite_border,
                      ),
                      color: Color(0xFFFACC5F),
                      onPressed: onTapFavorite,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}