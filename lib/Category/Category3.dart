import 'package:flutter/material.dart';

class Product {
  final String imageUrl;
  final String productName;
  final String price;

  Product({
    required this.imageUrl,
    required this.productName,
    required this.price,
  });
}

class Category3 extends StatefulWidget {
  final List<Product> products = [
    Product(
      imageUrl: 'assets/baking/bt1.png',
      productName: 'Baking Sheet',
      price: '\P 359',
    ),
    Product(
      imageUrl: 'assets/baking/bt2.png',
      productName: 'Bowl Scraper',
      price: '\P 250',
    ),
    Product(
      imageUrl: 'assets/baking/bt3.png',
      productName: 'Bundt Pan',
      price: '\P 489',
    ),
    Product(
      imageUrl: 'assets/baking/bt4.jpg',
      productName: 'Cake Pans',
      price: '\P 450',
    ),
    Product(
      imageUrl: 'assets/baking/bt5.png',
      productName: 'Measuring Cup',
      price: '\P 599',
    ),
    Product(
      imageUrl: 'assets/baking/bt6.png',
      productName: 'Muffin Pan',
      price: '\P 449',
    ),
    Product(
      imageUrl: 'assets/baking/bt7.jpg',
      productName: 'Pastry Brush',
      price: '\P 320',
    ),
    Product(
      imageUrl: 'assets/baking/bt8.jpg',
      productName: 'Pastry Cutter',
      price: '\P 320',
    ),
    Product(
      imageUrl: 'assets/baking/bt9.png',
      productName: 'Pizza Pan',
      price: '\P 470',
    ),
    Product(
      imageUrl: 'assets/baking/bt10.jpg',
      productName: 'Wire Rack',
      price: '\P 900',
    ),
  ];

  @override
  _Category3State createState() => _Category3State();
}

class _Category3State extends State<Category3> {
  List<bool> isLiked = List.generate(10, (index) => false);

  void showSnackBar(String text) {
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text(text),
        duration: Duration(seconds: 1),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xFFB28264),
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios_rounded),
          color: Color(0xFFF8F3EC),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        title: Text(
          "Baking Tools",
          style: TextStyle(
            color: Color(0xFFF8F3EC),
          ),
        ),
      ),
      body: Container(
        color: Color(0xFFF8F3EC),
        child: GridView.builder(
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2,
            crossAxisSpacing: 10.0,
            mainAxisSpacing: 10.0,
          ),
          itemCount: widget.products.length,
          itemBuilder: (context, index) {
            return _buildProductBox(
              imageUrl: widget.products[index].imageUrl,
              productName: widget.products[index].productName,
              price: widget.products[index].price,
              isLiked: isLiked[index],
              onTapImage: () {
                if (index == 0) {
                  Navigator.pushNamed(context, 'bt1');
                } else if (index == 1) {
                  Navigator.pushNamed(context, 'bt2');
                } else if (index == 2) {
                  Navigator.pushNamed(context, 'bt3');
                } else if (index == 3) {
                  Navigator.pushNamed(context, 'bt4');
                } else if (index == 4) {
                  Navigator.pushNamed(context, 'bt5');
                } else if (index == 5) {
                  Navigator.pushNamed(context, 'bt6');
                } else if (index == 6) {
                  Navigator.pushNamed(context, 'bt7');
                } else if (index == 7) {
                  Navigator.pushNamed(context, 'bt8');
                } else if (index == 8) {
                  Navigator.pushNamed(context, 'bt9');
                }else if (index == 9) {
                  Navigator.pushNamed(context, 'bt10');
                }
              },
              onTapFavorite: () {
                setState(() {
                  isLiked[index] = !isLiked[index];
                  showSnackBar(isLiked[index]
                      ? 'You liked ${widget.products[index].productName}'
                      : 'You unliked ${widget.products[index].productName}');
                });
              },
            );
          },
        ),
      ),
    );
  }

  Widget _buildProductBox({
    required String imageUrl,
    required String productName,
    required String price,
    required bool isLiked,
    required VoidCallback onTapImage,
    required VoidCallback onTapFavorite,
  }) {
    return GestureDetector(
      onTap: onTapImage,
      child: Container(
        padding: EdgeInsets.all(16.0),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10.0),
          boxShadow: [
            BoxShadow(
              color: Color(0xFFB28264),
              blurRadius: 5.0,
              offset: Offset(0, 2),
            ),
          ],
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 6.0),
            Center(
              child: Image.asset(
                imageUrl,
                width: 100,
                height: 100,
              ),
            ),
            SizedBox(height: 6.0),
            Text(
              productName,
              style: TextStyle(
                fontSize: 15.0,
                fontWeight: FontWeight.bold,
                color: Color(0xFFB28264),
              ),
            ),
            SizedBox(height: 1.0),
            Expanded(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    price,
                    style: TextStyle(fontSize: 16.0),
                  ),
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5),
                      color: Color(0xFF34312F),
                    ),
                    child: IconButton(
                      icon: Icon(
                        isLiked
                            ? Icons.favorite_rounded
                            : Icons.favorite_border,
                      ),
                      color: Color(0xFFFACC5F),
                      onPressed: onTapFavorite,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
