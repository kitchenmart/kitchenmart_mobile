import 'package:flutter/material.dart';

class homeappbar extends StatelessWidget{
  const homeappbar({super.key});

  @override
  Widget build(BuildContext context){
    return Container(
      color: const Color(0xFFF8F3EC),
      padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 25),
      child: Row(
          children: [
            const Padding(
              padding: EdgeInsets.only(left: 80),
              child: Text(
                  'Kitchen',
                  style: TextStyle(
                    fontSize: 25,
                    fontFamily: 'Roboto',
                    fontWeight: FontWeight.w600,
                    color: Color(0xFF34312F),
                    fontStyle: FontStyle.italic,
                  )),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 5),
              child: Image.asset('assets/logo.png',
                width: 60,
                height: 60,
              ),
            ),
            const Padding(
              padding: EdgeInsets.only(left: 5),
              child: Text(
                  'Mart',
                  style: TextStyle(
                    fontSize: 25,
                    fontFamily: 'Roboto',
                    fontWeight: FontWeight.w600,
                    color: Color(0xFF34312F),
                    fontStyle: FontStyle.italic,
                  )),
            )
          ]
      ),
    );
  }
}
