import 'package:flutter/material.dart';

class cartAppBar extends StatelessWidget {
  const cartAppBar({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
        color: const Color(0xFFB28264),
        padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 25),
        child: const Row(
          children: [
            Padding(
              padding: EdgeInsets.only(left: 20),
              child: Text(
                "My Cart",
                style: TextStyle(
                  fontSize: 23,
                  fontWeight: FontWeight.bold,
                  color: Color(0xFFF8F3EC),
                ),
              ),
            ),
            Spacer(),
            Icon(
              Icons.delete,
              size: 25,
              color: Color(0xFFF8F3EC),
            )
          ],
        )
    );
  }
}
