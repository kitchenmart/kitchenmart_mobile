import 'package:flutter/material.dart';

class accountapp extends StatelessWidget{
  const accountapp({super.key});

  @override
  Widget build(BuildContext context){
    return Container(
      color: const Color(0xFFB28264),
      padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 25),
      child: Row(
          children: [
            Padding(
              padding: const EdgeInsets.only(left: .5),
                child: InkWell(
                  onTap: () {
                    Navigator.pushNamed(context, "LoginPage");
                  },
                  child: const Icon(
                Icons.account_circle_rounded,
                size: 50,
                color: Color(0xFFF8F3EC),
              )
            ),
        ),
            const Padding(
              padding: EdgeInsets.only(left: 15),
              child: Text(
                  'First Name, Last Name',
                  style: TextStyle(
                    fontSize: 25,
                    fontFamily: 'Roboto',
                    fontWeight: FontWeight.w600,
                    color: Color(0xFFF8F3EC),
                    fontStyle: FontStyle.italic,
                  )),
            )
            ]
      )
      );
  }
}
