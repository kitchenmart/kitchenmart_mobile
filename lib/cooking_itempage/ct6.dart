import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

class ct6 extends StatefulWidget {
  @override
  _ct6State createState() => _ct6State();
}

class _ct6State extends State<ct6> {
  PageController _pageController = PageController();
  int _currentPage = 0;

  String _selectedImagePath = "assets/category/ct6.jpg";

  // Quantity
  int quantityCount = 0;

  void decrementQuantity() {
    if (quantityCount > 1) {
      setState(() {
        quantityCount--;
      });
    }
  }

  void incrementQuantity() {
    setState(() {
      quantityCount++;
    });
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios_rounded,
            color: Color(0xFF34312F),
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        actions: [
          IconButton(
            icon: Icon(
              Icons.shopping_cart,
              color: Color(0xFF34312F),
            ),
            onPressed: () {
              Navigator.pushNamed(context, 'cartPage');
            },
          ),
        ],
        backgroundColor: Colors.transparent,
        elevation: 0,
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            children: [
              SizedBox(height: 15),
              Container(
                height: 400,
                width: 400,
                child: Stack(
                  alignment: Alignment.bottomRight,
                  children: [
                    PageView(
                      controller: _pageController,
                      onPageChanged: (index) {
                        setState(() {
                          _currentPage = index;
                        });
                      },
                      children: [
                        _buildImageContainer("assets/category/ct6.jpg"),
                        _buildImageContainer("assets/category2nd/ct6.jpg"),
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        "${_currentPage + 1}/2",
                        style: TextStyle(
                          fontSize: 16,
                          color: Color(0xFF34312F),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(height: 15),
              Container(
                width: double.infinity,
                padding: EdgeInsets.symmetric(vertical: 30, horizontal: 20),
                decoration: BoxDecoration(
                  color: Color(0xFFFFFFFF),
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(35),
                    topRight: Radius.circular(35),
                  ),
                  boxShadow: [
                    BoxShadow(
                      color: Color(0xFFB28264).withOpacity(0.4),
                      blurRadius: 10,
                      spreadRadius: 1,
                    ),
                  ],
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "\P 240",
                          style: TextStyle(
                            fontSize: 25,
                            fontWeight: FontWeight.w500,
                            color: Color(0xFFB28264),
                          ),
                        ),
                        SizedBox(height: 5),
                        Text(
                          "Spatula",
                          style: TextStyle(
                            fontSize: 28,
                            fontWeight: FontWeight.bold,
                            color: Color(0xFF34312F),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: 5),
                    Text(
                      "Classic Slotted Spatula, One Size, Non-Stick",
                      style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.w300,
                        color: Color(0xFF34312F),
                      ),
                    ),
                    SizedBox(height: 15),
                    Text(
                      "Details",
                      style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.w400,
                        color: Color(0xFF34312F),
                        fontStyle: FontStyle.italic,
                      ),
                    ),
                    Divider(
                      height: 2,
                      thickness: 2,
                      color: Colors.grey,
                      indent: 0,
                      endIndent: 0,
                    ),
                    Text(
                      " Color: Black",
                      style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.w300,
                        color: Color(0xFF34312F),
                      ),
                    ),
                    Divider(
                      height: 2,
                      thickness: 1,
                      color: Colors.grey,
                      indent: 0,
                      endIndent: 0,
                    ),
                    SizedBox(height: 3),
                    Text(
                      "Weight: .32 pounds",
                      style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.w300,
                        color: Color(0xFF34312F),
                      ),
                    ),
                    Divider(
                      height: 2,
                      thickness: 2,
                      color: Colors.grey,
                      indent: 0,
                      endIndent: 0,
                    ),
                    SizedBox(height: 30),
                    Container(
                      alignment: Alignment.centerLeft,
                      child: RatingBar.builder(
                        initialRating: 3,
                        minRating: 1,
                        direction: Axis.horizontal,
                        allowHalfRating: true,
                        itemCount: 5,
                        itemSize: 20,
                        itemPadding: const EdgeInsets.symmetric(horizontal: 2),
                        itemBuilder: (context, _) =>
                        const Icon(
                          Icons.star,
                          color: Color(0xFFFACC5F),
                        ),
                        onRatingUpdate: (rating) {},
                      ),
                    ),
                    SizedBox(height: 20),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Container(
                          decoration: BoxDecoration(
                            color: Color(0xFFF8F3EC),
                            shape: BoxShape.circle,
                          ),
                          child: IconButton(
                            icon: Icon(Icons.remove),
                            onPressed: decrementQuantity,
                          ),
                        ),
                        SizedBox(
                          width: 40,
                          child: Center(
                            child: Text(
                              quantityCount.toString(),
                              style: TextStyle(
                                color: Color(0xFF34312F),
                                fontSize: 18,
                              ),
                            ),
                          ),
                        ),
                        Container(
                          decoration: BoxDecoration(
                            color: Color(0xFFF8F3EC),
                            shape: BoxShape.circle,
                          ),
                          child: IconButton(
                            icon: Icon(Icons.add),
                            onPressed: incrementQuantity,
                          ),
                        )
                      ],
                    )

                  ],
                ),
              ),
            ],
          ),
        ),
      ),
      bottomNavigationBar: ItemBottomBar(),
    );
  }

  Widget _buildImageContainer(String imagePath) {
    return GestureDetector(
      onTap: () {
        setState(() {
          _selectedImagePath = imagePath;
        });
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (_) => FullScreenImage(
              imagePath: _selectedImagePath,
            ),
          ),
        );
      },
      child: Container(
        height: 400,
        width: 350,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5),
          border: Border.all(
            color: Color(0xFFB28264),
            width: 5,
          ),
        ),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(5),
          child: Image.asset(
            imagePath,
            fit: BoxFit.cover,
            height: 350,
            width: 350,
          ),
        ),
      ),
    );
  }
}

class FullScreenImage extends StatelessWidget {
  final String imagePath;

  FullScreenImage({required this.imagePath});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GestureDetector(
        onTap: () {
          Navigator.pop(context);
        },
        child: Center(
          child: Hero(
            tag: 'image_hero',
            child: Image.asset(
              imagePath,
              fit: BoxFit.contain,
            ),
          ),
        ),
      ),
    );
  }
}

class ItemBottomBar extends StatelessWidget {
  const ItemBottomBar({Key? key});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 70,
      padding: const EdgeInsets.symmetric(horizontal: 20),
      decoration: BoxDecoration(
        color: const Color(0xFF34312F),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        children: [
          ElevatedButton.icon(
            onPressed: () {},
            style: ButtonStyle(
              minimumSize: MaterialStateProperty.all(const Size(175.0, 50.0)),
              backgroundColor: MaterialStateProperty.all(
                const Color(0xFFFACC5F),
              ),
              padding: MaterialStateProperty.all(
                const EdgeInsets.symmetric(vertical: 13, horizontal: 15),
              ),
              shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20),
                ),
              ),
            ),
            icon: const Icon(Icons.money_outlined),
            label: const Text(
              "Buy Now",
              style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          const SizedBox(width: 16.0),
          InkWell(
            onTap: () {},
            child: ElevatedButton.icon(
              onPressed: () {},
              style: ButtonStyle(
                minimumSize: MaterialStateProperty.all(const Size(175.0, 50.0)),
                backgroundColor: MaterialStateProperty.all(
                  const Color(0xFFB28264),
                ),
                padding: MaterialStateProperty.all(
                  const EdgeInsets.symmetric(vertical: 13, horizontal: 15),
                ),
                shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                  RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20),
                  ),
                ),
              ),
              icon: const Icon(Icons.shopping_cart_outlined),
              label: const Text(
                "Add To Cart",
                style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

void main() {
  runApp(MaterialApp(
    home: ct6(),
  ));
}
