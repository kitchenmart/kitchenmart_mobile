import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:kitchen_mart/CartItem.dart';
import 'package:kitchen_mart/pages/CartPage.dart';

class cc2 extends StatefulWidget {
  @override
  _cc2State createState() => _cc2State();
}

class _cc2State extends State<cc2> {
  PageController _pageController = PageController();
  int _currentPage = 0;

  String _selectedImagePath = "assets/Cutting/cc2.png";

  // Quantity
  int quantityCount = 1;

  void decrementQuantity() {
    if (quantityCount > 1) {
      setState(() {
        quantityCount--;
      });
    }
  }

  void incrementQuantity() {
    setState(() {
      quantityCount++;
    });
  }

  List<CartItem> cartItems = [];

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  void _showAddedToCartDialog() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // Show the dialog
        Future.delayed(Duration(seconds: 1), () {
          Navigator.of(context).pop();
        });

        return AlertDialog(
          backgroundColor: Color(0xFF34312F),
          content: Text(
            "Added to Cart",
            style: TextStyle(
              color: Color(0xFFF8F3EC),
            ),
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios_rounded,
            color: Color(0xFF34312F),
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        actions: [
          IconButton(
            icon: Icon(
              Icons.shopping_cart,
              color: Color(0xFF34312F),
            ),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => CartPage(cartItems: cartItems),
                ),
              );
            },
          ),
        ],
        backgroundColor: Colors.transparent,
        elevation: 0,
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            children: [
              SizedBox(height: 15),
              Container(
                height: 400,
                width: 400,
                child: Stack(
                  alignment: Alignment.bottomRight,
                  children: [
                    PageView(
                      controller: _pageController,
                      onPageChanged: (index) {
                        setState(() {
                          _currentPage = index;
                        });
                      },
                      children: [
                        _buildImageContainer("assets/Cutting/cc2.png"),
                        _buildImageContainer("assets/cutting2nd/cc2.png"),
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        "${_currentPage + 1}/2",
                        style: TextStyle(
                          fontSize: 16,
                          color: Color(0xFF34312F),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(height: 15),
              Container(
                width: double.infinity,
                padding: EdgeInsets.symmetric(vertical: 30, horizontal: 20),
                decoration: BoxDecoration(
                  color: Color(0xFFFFFFFF),
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(35),
                    topRight: Radius.circular(35),
                  ),
                  boxShadow: [
                    BoxShadow(
                      color: Color(0xFFB28264).withOpacity(0.4),
                      blurRadius: 10,
                      spreadRadius: 1,
                    ),
                  ],
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "\P 399",
                          style: TextStyle(
                            fontSize: 25,
                            fontWeight: FontWeight.w500,
                            color: Color(0xFFB28264),
                          ),
                        ),
                        SizedBox(height: 5),
                        Text(
                          "Biscuit Cutter",
                          style: TextStyle(
                            fontSize: 28,
                            fontWeight: FontWeight.bold,
                            color: Color(0xFF34312F),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: 5),
                    Text(
                      "Round Cookie Biscuit Cutter Set, 6 Graduated Circle Pastry Cutters, Circle Cutter Ring Molds",
                      style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.w300,
                        color: Color(0xFF34312F),
                      ),
                    ),
                    SizedBox(height: 15),
                    Text(
                      "Details",
                      style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.w400,
                        color: Color(0xFF34312F),
                        fontStyle: FontStyle.italic,
                      ),
                    ),
                    Divider(
                      height: 2,
                      thickness: 2,
                      color: Colors.grey,
                      indent: 0,
                      endIndent: 0,
                    ),
                    Text(
                      "Color: Assorted",
                      style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.w300,
                        color: Color(0xFF34312F),
                      ),
                    ),
                    Divider(
                      height: 2,
                      thickness: 1,
                      color: Colors.grey,
                      indent: 0,
                      endIndent: 0,
                    ),
                    SizedBox(height: 3),
                    Text(
                      "Weight: 5.3 ounces",
                      style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.w300,
                        color: Color(0xFF34312F),
                      ),
                    ),
                    Divider(
                      height: 2,
                      thickness: 2,
                      color: Colors.grey,
                      indent: 0,
                      endIndent: 0,
                    ),
                    SizedBox(height: 30),
                    Container(
                      alignment: Alignment.centerLeft,
                      child: RatingBar.builder(
                        initialRating: 3,
                        minRating: 1,
                        direction: Axis.horizontal,
                        allowHalfRating: true,
                        itemCount: 5,
                        itemSize: 20,
                        itemPadding: const EdgeInsets.symmetric(horizontal: 2),
                        itemBuilder: (context, _) =>
                        const Icon(
                          Icons.star,
                          color: Color(0xFFFACC5F),
                        ),
                        onRatingUpdate: (rating) {},
                      ),
                    ),
                    SizedBox(height: 20),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
      bottomNavigationBar: Container(
        height: 70,
        padding: const EdgeInsets.symmetric(horizontal: 20),
        decoration: BoxDecoration(
          color: const Color(0xFF34312F),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.max,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Container(
                  decoration: BoxDecoration(
                    color: Color(0xFFF8F3EC),
                    shape: BoxShape.circle,
                  ),
                  child: IconButton(
                    icon: Icon(Icons.remove),
                    onPressed: decrementQuantity,
                  ),
                ),
                SizedBox(
                  width: 40,
                  child: Center(
                    child: Text(
                      quantityCount.toString(),
                      style: TextStyle(
                        color: Color(0xFFF8F3EC),
                        fontSize: 18,
                      ),
                    ),
                  ),
                ),
                Container(
                  decoration: BoxDecoration(
                    color: Color(0xFFF8F3EC),
                    shape: BoxShape.circle,
                  ),
                  child: IconButton(
                    icon: Icon(Icons.add),
                    onPressed: incrementQuantity,
                  ),
                )
              ],
            ),
            SizedBox(width: 16.0),
            ElevatedButton.icon(
              onPressed: () {
                final cartItem = CartItem(
                  productName: "Biscuit Cutter",
                  productImage: _selectedImagePath,
                  productPrice: 399.0,
                  quantity: quantityCount,
                );

                // Add the item to the cart
                setState(() {
                  cartItems.add(cartItem);
                });

                // Show the "Added to Cart" dialog
                _showAddedToCartDialog();

              },
              style: ButtonStyle(
                minimumSize: MaterialStateProperty.all(const Size(175.0, 50.0)),
                backgroundColor: MaterialStateProperty.all(
                  const Color(0xFFB28264),
                ),
                padding: MaterialStateProperty.all(
                  const EdgeInsets.symmetric(vertical: 13, horizontal: 15),
                ),
                shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                  RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20),
                  ),
                ),
              ),
              icon: const Icon(Icons.shopping_cart_outlined),
              label: const Text(
                "Add To Cart",
                style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildImageContainer(String imagePath) {
    return GestureDetector(
      onTap: () {
        setState(() {
          _selectedImagePath = imagePath;
        });
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (_) => FullScreenImage(
              imagePath: _selectedImagePath,
            ),
          ),
        );
      },
      child: Container(
        height: 400,
        width: 350,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5),
          border: Border.all(
            color: Color(0xFFB28264),
            width: 5,
          ),
        ),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(5),
          child: Image.asset(
            imagePath,
            fit: BoxFit.cover,
            height: 350,
            width: 350,
          ),
        ),
      ),
    );
  }
}

class FullScreenImage extends StatelessWidget {
  final String imagePath;

  FullScreenImage({required this.imagePath});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GestureDetector(
        onTap: () {
          Navigator.pop(context);
        },
        child: Center(
          child: Hero(
            tag: 'image_hero',
            child: Image.asset(
              imagePath,
              fit: BoxFit.contain,
            ),
          ),
        ),
      ),
    );
  }
}

class ItemBottomBar extends StatelessWidget {
  const ItemBottomBar({Key? key});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 70,
      padding: const EdgeInsets.symmetric(horizontal: 20),
      decoration: BoxDecoration(
        color: const Color(0xFF34312F),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        children: [
          ElevatedButton.icon(
            onPressed: () {},
            style: ButtonStyle(
              minimumSize: MaterialStateProperty.all(const Size(175.0, 50.0)),
              backgroundColor: MaterialStateProperty.all(
                const Color(0xFFFACC5F),
              ),
              padding: MaterialStateProperty.all(
                const EdgeInsets.symmetric(vertical: 13, horizontal: 15),
              ),
              shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20),
                ),
              ),
            ),
            icon: const Icon(Icons.money_outlined),
            label: const Text(
              "Buy Now",
              style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          const SizedBox(width: 16.0),
          InkWell(
            onTap: () {},
            child: ElevatedButton.icon(
              onPressed: () {},
              style: ButtonStyle(
                minimumSize: MaterialStateProperty.all(const Size(175.0, 50.0)),
                backgroundColor: MaterialStateProperty.all(
                  const Color(0xFFB28264),
                ),
                padding: MaterialStateProperty.all(
                  const EdgeInsets.symmetric(vertical: 13, horizontal: 15),
                ),
                shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                  RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20),
                  ),
                ),
              ),
              icon: const Icon(Icons.shopping_cart_outlined),
              label: const Text(
                "Add To Cart",
                style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

void main() {
  runApp(MaterialApp(
    home: cc2(),
  ));
}
