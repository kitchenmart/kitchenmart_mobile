import 'package:flutter/material.dart';

class ItemWidget extends StatelessWidget {
  final List<String> categoryNames = [
    "Appliances",
    "Specialty Tools",
    "Baking Tools",
    "Cooking Tools",
    "Cutting and Chopping",
    "Mixing and Preparatory Tools",
  ];

  final List<String> descriptions = [
    "Appliances for modern kitchen convenience.",
    "Specialty tools for unique culinary creations.",
    "Essential tools for all your baking needs.",
    "Cooking tools to enhance your culinary skills.",
    "Tools for precise cutting and efficient chopping.",
    "Essential tools for mixing and food preparation.",
  ];


  ItemWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GridView.count(
      childAspectRatio: 0.72,
      physics: const NeverScrollableScrollPhysics(),
      crossAxisCount: 2,
      shrinkWrap: true,
      children: [
        for (int i = 0; i < categoryNames.length; i++)
          Container(
            height: 30,
            padding: const EdgeInsets.all(10),
            margin:
            const EdgeInsets.symmetric(vertical: 8, horizontal: 10),
            decoration: const BoxDecoration(
              color: Color(0xFFFFFFFF),
              borderRadius: BorderRadius.all(Radius.circular(20)),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                InkWell(
                  onTap: () {
                    Navigator.pushNamed(context, "Category${i + 1}");
                  },
                  child: Container(
                    margin: const EdgeInsets.all(10),
                    child: Image.asset(
                      'assets/categories${i + 1}.png',
                      height: 130,
                      width: 130,
                    ),
                  ),
                ),
                SizedBox(height: 8),
                Text(
                  categoryNames[i],
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                    color: Color(0xFF34312F),
                  ),
                ),
                SizedBox(height: 8),
                Text(
                  descriptions[i],
                  style: TextStyle(
                    fontSize: 14,
                    color: Colors.grey,
                  ),
                ),
              ],
            ),
          )
      ],
    );
  }
}
