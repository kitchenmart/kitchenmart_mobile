import 'package:flutter/material.dart';

class SaleWidget extends StatefulWidget {
  final List<String> productNames = [
    "Food Processor",
    "Cutting Board",
    "Chef's Knife",
    "Rolling Pin",
    "Whisk",
    "Potato Masher",
    "Garlic Press",
  ];

  final List<String> descriptions = [
    "Appliances",
    "Cutting and Chopping",
    "Cutting and Chopping",
    "Baking Tools",
    "Baking Tools",
    "Specialty Tools",
    "Specialty Tools",
  ];

  final List<String> prices = [
    "P 199",
    "P 189",
    "P 99",
    "P 89",
    "P 100",
    "P 160",
    "P 180",
  ];

  SaleWidget({Key? key}) : super(key: key);

  @override
  _SaleWidgetState createState() => _SaleWidgetState();
}

class _SaleWidgetState extends State<SaleWidget> {
  List<bool> isLiked = List.generate(7, (index) => false);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Row(
        children: [
          for (int i = 1; i <= 7; i++)
            Container(
              margin: EdgeInsets.only(top: 10, bottom: 10, left: 10),
              padding: EdgeInsets.symmetric(horizontal: 10),
              height: 180,
              decoration: BoxDecoration(
                color: Color(0xFFFFFFFF),
                borderRadius: BorderRadius.circular(10),
                boxShadow: [
                  BoxShadow(
                    color: Color(0xFFB28264).withOpacity(0.3),
                    blurRadius: 5,
                    spreadRadius: 1,
                  ),
                ],
              ),
              child: Row(
                children: [
                  Stack(
                    alignment: Alignment.center,
                    children: [
                      Container(
                        margin: EdgeInsets.only(top: 20, right: 70),
                        height: 110,
                        width: 120,
                        decoration: BoxDecoration(
                          color: Color(0xFFB28264),
                          borderRadius: BorderRadius.circular(10),
                        ),
                      ),
                      InkWell(
                        onTap: () {
                          Navigator.pushNamed(context, "Items$i");
                        },
                        child: Image.asset(
                          'assets/below$i.jpg',
                          height: 150,
                          width: 150,
                          fit: BoxFit.contain,
                        ),
                      ),
                    ],
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(vertical: 30),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          widget.productNames[i - 1],
                          style: TextStyle(
                            color: Color(0xFF34312F),
                            fontSize: 23,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                        SizedBox(height: 5),
                        Text(
                          widget.descriptions[i - 1],
                          style: TextStyle(
                            color: Color(0xFF34312F).withOpacity(0.8),
                            fontSize: 16,
                          ),
                        ),
                        Spacer(),
                        Row(
                          children: [
                            Text(
                              widget.prices[i - 1],
                              style: TextStyle(
                                color: Color(0xFFB28264),
                                fontSize: 22,
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                            SizedBox(width: 70),
                            InkWell(
                              onTap: () {
                                setState(() {
                                  isLiked[i - 1] = !isLiked[i - 1];
                                  _showLikeMessage(i - 1);
                                });
                              },
                              child: Container(
                                padding: EdgeInsets.all(10),
                                decoration: BoxDecoration(
                                  color: Color(0xFF34312F),
                                  borderRadius: BorderRadius.circular(10),
                                ),
                                child: Icon(
                                  isLiked[i - 1]
                                      ? Icons.favorite_rounded
                                      : Icons.favorite_outline,
                                  color: Color(0xFFFACC5F),
                                  size: 25,
                                ),
                              ),
                            ),
                          ],
                        )
                      ],
                    ),
                  )
                ],
              ),
            )
        ],
      ),
    );
  }

  void _showLikeMessage(int index) {
    final productName = widget.productNames[index];
    final liked = isLiked[index] ? "liked" : "unliked";
    final message = "You $liked $productName";
    showDialog(
      context: context,
      builder: (BuildContext context) {
        Future.delayed(Duration(seconds: 1), () {
          Navigator.of(context).pop();
        });

        return AlertDialog(
          content: Text(
            message,
            style: TextStyle(color: Color(0xFFF8F3EC)),
          ),
          backgroundColor: Color(0xFF34312F),
        );
      },
    );
  }
}
