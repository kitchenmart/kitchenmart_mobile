import 'package:flutter/material.dart';

class cartbottom extends StatelessWidget{
  const cartbottom({super.key});

  @override
  Widget build(BuildContext context){
    return Container(
      height: 65,
      padding: const EdgeInsets.symmetric(horizontal: 20),
      decoration: const BoxDecoration(
          color: Color(0xFF34312F),
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(25),
            topRight: Radius.circular(25),
          )
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          InkWell(
            onTap: () {
              Navigator.pushNamed(context, "homepage");
            },
            child: const Icon(
              Icons.home,
              color: Color(0xFFF8F3EC),
              size: 32,
            ),),

          InkWell(
            onTap: () {
              Navigator.pushNamed(context, "notificationPage");
            },
            child:const Icon(
              Icons.notifications,
              color: Color(0xFFF8F3EC),
              size: 32,
            ),
          ),

          InkWell(
            onTap: () {
              Navigator.pushNamed(context, "CartPage");
            },
            child: const Icon(
              Icons.shopping_cart,
              color: Color(0xFFB28264),
              size: 32,
            ),
          ),

          InkWell(
            onTap: () {
              Navigator.pushNamed(context, "accountPage");
            },
            child: const Icon(
              Icons.person,
              color: Color(0xFFF8F3EC),
              size: 32,
            ),
          ),
        ],
      ),
    );
  }
}